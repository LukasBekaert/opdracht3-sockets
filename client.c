#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
void error(const char *msg)
{
    perror(msg);
    exit(0);
}
void FileBuffer( char* argv,char**fileBuffer) {
 FILE    *file;
 long    numbytes;
 file = fopen(argv, "r");
 if(file == NULL)
 error("no such file found.");
 fseek(file, 0L, SEEK_END);
 numbytes = ftell(file);
 fseek(file, 0L, SEEK_SET);
*fileBuffer = (char*)calloc(numbytes, sizeof(char));
 if(*fileBuffer == NULL)
 error("cant read from file");

 fread(*fileBuffer, sizeof(char), numbytes, file);
 fclose(file);
}
int main(int argc, char *argv[])
{

    char    *fileBuffer;

    int sockfd, portno, n;

    struct sockaddr_in serv_addr;
    struct hostent *server;


   FileBuffer(argv[3],&fileBuffer);


    portno = atoi(argv[2]);
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) 
        error("cant open socket");
    server = gethostbyname(argv[1]);
    if (server == NULL) {
        fprintf(stderr,"no such host\n");
        exit(0);
    }
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr, 
         (char *)&serv_addr.sin_addr.s_addr,
         server->h_length);
    serv_addr.sin_port = htons(portno);
    if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) 
        error("cant connect");
    printf("file:%s is being sent. \r\n",argv[3]);

    n = write(sockfd,fileBuffer,strlen(fileBuffer));

    printf("Sending %s",fileBuffer);
    free(fileBuffer);

    if (n < 0) 
         error("cant write to socket");

    close(sockfd);
    return 0;
}
